function(doc) {
	if(doc.type === "paper" && doc.created_at) {
		emit(doc.created_at, doc);
	}
}
