function(doc) {
  if(doc.type == "paper") {
    emit(doc.name, doc);
  } else if(doc.type == "comment") {
    emit(doc.parent, doc);
  }
}
