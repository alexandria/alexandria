function(head, req) {
	var ddoc = this;
	var Mustache = require("vendor/couchapp/lib/mustache");
	var List = require("vendor/couchapp/lib/list");
	var path = require("vendor/couchapp/lib/path").init(req);

	provides("html", function() {
		var papers = [], row;
		while(row = getRow()) {
			papers.push(row.value);
		}
		var stash = {
			papers: papers,
			title: "Alexandria",
			assets : path.asset()
		}
		return Mustache.to_html(ddoc.templates.author, stash, ddoc.templates.partials, List.send);
	});
  
}
