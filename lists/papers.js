function(head, req) {
	var ddoc = this;
	var Mustache = require("vendor/couchapp/lib/mustache");
	var List = require("vendor/couchapp/lib/list");
	var path = require("vendor/couchapp/lib/path").init(req);

	provides("html", function() {
		var list = [], row;
		while(row = getRow()) {
			list.push(row.value);
		}
		var stash = {
			papers: list,
			title: "Alexandria",
			assets : path.asset()
		}
		return Mustache.to_html(ddoc.templates.papers, stash, ddoc.templates.partials, List.send);
	});
  
}
