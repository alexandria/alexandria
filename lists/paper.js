function(doc, req) {  
	var ddoc = this;
	var Mustache = require("vendor/couchapp/lib/mustache");
	var path = require("vendor/couchapp/lib/path").init(req);
	var List = require("vendor/couchapp/lib/list");

	provides("html", function() {
		var paper, comments = [], row;
		while(row = getRow()) {
			if(row.value.type == "paper") {
				paper = row.value;
			} else {
				comments.push(row.value);
			}
		}

		return Mustache.to_html(ddoc.templates.paper, 
			{
				assets: path.asset(), 
				paper: paper,
				comments: comments
			}, ddoc.templates.partials, List.send);
	});
}
