## Alexandria

Alexandria is a couchapp designed to:

	- share papers
	- comment them
	- get recommendations
	- keep a readlist

### Install

Dependencies: CouchDB, [couchapp](http://github.com/couchapp/couchapp)

    $ couchapp push http://localhost:5984/alexandria
    [INFO] Visit your CouchApp here:
    http://localhost:5984/library/_design/papers/_list/papers/papers?descending=true

### Nice urls and CSS

Add "127.0.0.1 alexandria.local" to /etc/hosts, and
`alexandria.local:5984 = /library/_design/papers/_rewrite` under
[vhost] in your `etc/couchdb/local.ini`
file. [Enjoy!](http://alexandria.local:5984/)

### Status

Currently, it only displays a few papers on a simple page, and handle
search by tags and authors. Release early, release often.

A paper is a simple document in CouchDB, for example:

    {
    	"_id": "7a40c59eef2f7ded9d5fc1afd1002c30",
    	"_rev": "5-40834d249ff8e4587067da5093d98b93",
    	"type": "paper",
    	"author": ["Philip Wadler"],
    	"name": "Monads for functional programming",
    	"org": "University of Glasgow",
    	"tags": ["monads", "introduction"],
    	"date": "Mon May 01 1995 00:00:00",
    	"created_at": "Thu Oct 08 2010 18:16:09",
    	"abstract": "The use of monads to structure functional
    	programs is described."
    }

I only have three views, papers, authors and tags, the corresponding
list functions. Rendering code is in templates/.
